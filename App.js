import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, Touchable, TouchableOpacity, Alert, Animated, ToastAndroid } from 'react-native';
import Paho from 'paho-mqtt'
import { useEffect, useState, useRef } from 'react';
import * as Device from 'expo-device';



export default function App() {

  const [connectMqtt, setConnectMqtt] = useState(null)
  const [errorMqtt, setErrorMqtt] = useState(null)
  const [topAlert, setTopAlert] = useState(40)
  const connectData = {
    host: '192.168.1.67', 
    port: 9001,
    clientId: 'LastLowApp'
  }

  const client = new Paho.Client(connectData.host, Number(connectData.port), connectData.clientId);

  const fade = useRef(new Animated.Value(-120)).current

  useEffect(() => {
    connection()
  }, [])

  const connection = async () => {
    try {
      client.connect({
        onSuccess() {
          setConnectMqtt(true)
          menuDown(fade);
          setTimeout(() => menuUp(fade), 1500)
        },
        onFailure(e) {
          console.log(e);
          setConnectMqtt(false)
          setErrorMqtt(`Code: ${e.errorCode}/ Error: ${e.errorMessage}`)
          menuDown(fade);
        }
      })
    } catch (e) {
      console.log('catch');
      console.log(e);
    }
  }

  const menuUp = (fadeAnim) => {
    Animated.timing(
      fadeAnim,
      {
        toValue: -120,
        duration: 300,
        useNativeDriver: true
      }
    ).start();
  }

  const menuDown = (fadeAnim) => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 0,
        duration: 300,
        useNativeDriver: true
      }
    ).start();
  }


  const sendHello = () => {
    let message = new Paho.Message([Device.brand, Device.modelName, Device.deviceName, Device.osName, Device.osVersion, Device.totalMemory].join(', '));
    message.destinationName = "test";
    client.send(message)
    ToastAndroid.showWithGravityAndOffset(
      "Information sent",
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
      25,
      200
    )
  }

  const styles = StyleSheet.create({
    statusBar: {
      color: '#fff',
      backgroundColor: '#000'
    },
    alert: {
      position: 'absolute',
      top: 50,
      transform: [
        {
          translateY: fade
        }
      ],
      zIndex: 2,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 20,
      width: '90%',
      backgroundColor: '#3AC0A0',
    },
    alertError: {
      backgroundColor: '#FF616D'
    },
    container: {
      flex: 1,
      backgroundColor: '#14142B',
      alignItems: 'center',
    },
    button: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      bottom: 0,
      width: '100%',
      height: 60,
      backgroundColor: '#5200BA'
    },
    test: {
      backgroundColor: '#757575'
    },
    text: {
      color: '#fff',
      fontSize: 16,
      fontWeight: 'bold'
    },
    info: {
      width: '90%',
      borderRadius: 20,
      backgroundColor: '#1F1F36',
      marginBottom: 15
    },
    infoItem: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 30
    },
    infoText: {
      color: '#fff',
      fontWeight: 'bold',
      fontSize: 16
    }
  });

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.alert, !connectMqtt ? styles.alertError : null]}>
        <Text style={{ color: '#fff', fontSize: 20, padding: 10, fontWeight: 'bold' }}>{connectMqtt ? 'Success' : 'Error'}</Text>
        <Text style={{ color: '#fff', fontSize: 16, paddingBottom: 5 }}>{connectMqtt ? `Connection to ${connectData.host} is successful` : errorMqtt}</Text>
      </Animated.View>

      <View style={{ backgroundColor: '#393969',  width: '100%', alignItems: 'center',  marginBottom: 20,}}>
        <Text style={{ color: '#fff', paddingTop: 50, paddingBottom: 20, fontSize: 24, fontWeight: 'bold' }}>Device information</Text>
      </View>
      <View style={styles.info}>
        <View style={styles.infoItem}>
          <Text style={styles.infoText}>Brand :</Text>
          <Text style={styles.infoText}>{Device.brand}</Text>
        </View>
      </View>
      <View style={styles.info}>
        <View style={styles.infoItem}>
          <Text style={styles.infoText}>Modal name :</Text>
          <Text style={styles.infoText}>{Device.modelName}</Text>
        </View>
      </View>
      <View style={styles.info}>
        <View style={styles.infoItem}>
          <Text style={styles.infoText}>Device name :</Text>
          <Text style={styles.infoText}>{Device.deviceName}</Text>
        </View>
      </View>
      <View style={styles.info}>
        <View style={styles.infoItem}>
          <Text style={styles.infoText}>Name OS :</Text>
          <Text style={styles.infoText}>{Device.osName}</Text>
        </View>
      </View>
      <View style={styles.info}>
        <View style={styles.infoItem}>
          <Text style={styles.infoText}>Version OS :</Text>
          <Text style={styles.infoText}>{Device.osVersion}</Text>
        </View>
      </View>
      <View style={styles.info}>
        <View style={styles.infoItem}>
          <Text style={styles.infoText}>Amount of RAM :</Text>
          <Text style={styles.infoText}>{(Device.totalMemory / (1000 * 1000 * 1000)).toFixed(2)} Gb</Text>
        </View>
      </View>

      <View style={[styles.info, { marginTop: 20 }]}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, paddingVertical: 20}}>
          <Text style={styles.infoText}>HostUri :</Text>
          <Text style={styles.infoText}>{connectData.host}:{String(connectData.port)}</Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, paddingVertical: 20}}>
          <Text style={styles.infoText}>ClientId (userName) :</Text>
          <Text style={styles.infoText}>{connectData.clientId}</Text>
        </View>
      </View>
      <TouchableOpacity onPress={connectMqtt ? sendHello : connection} disabled={connectMqtt === null ? true : false} style={[styles.button, !connectMqtt ? styles.test : null]}>
        <Text style={styles.text}>{connectMqtt === false ? 'Refresh connection' : 'Send information about phone'}</Text>
      </TouchableOpacity>
      <StatusBar style="light" />
    </View>
  );
}


